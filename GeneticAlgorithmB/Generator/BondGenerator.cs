﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithmB.Generator
{
    public class BondGenerator
    {
        Random BondRandomizer;
        uint NumIntegralElements = 0;
        public List<List<uint>> Areas { get; private set; } = null;

        public BondGenerator(uint numIntegralElements)
        {
            this.BondRandomizer = new Random();
            this.NumIntegralElements = numIntegralElements;
        }

        public byte[,] GenerateAdjacencyMatrix()
        {
            byte[,] matrix = new byte[this.NumIntegralElements, this.NumIntegralElements];

            var areasElements = GetIntegralElementsAreas(this.NumIntegralElements);

            for (int i=0; i < areasElements.Count; i++)
            {
                for (int j = 0; j < areasElements[i].Count; j++)
                {
                    for (int k = j + 1; k < areasElements[i].Count; k++)
                    {
                        matrix[areasElements[i][j], areasElements[i][k]] = 1;
                    }
                }
            }

            this.Areas = areasElements;

            for (int i = 0; i < this.NumIntegralElements; i++)
            {
                for (int j = i; j < this.NumIntegralElements; j++)
                {
                    if (i == j) matrix[i, j] = 1;
                }
            }

            return matrix;
        }

        private List<List<uint>> GetIntegralElementsAreas(uint numIntegralElements)
        {
            List<uint> baseIntegralElements = new List<uint>();
            for (uint i=0; i < numIntegralElements; i++)
            {
                baseIntegralElements.Add(i);
            }
            List<List<uint>> areas = new List<List<uint>>();

            while (baseIntegralElements.Count > 2)
            {
                int countAreasElement = this.BondRandomizer.Next(2, baseIntegralElements.Count);
                List<uint> currentElements = new List<uint>();
                for (int i=0; i < countAreasElement; i++)
                {
                    if (baseIntegralElements.Count < 2) break;
                    int elementIndex = this.BondRandomizer.Next(0, baseIntegralElements.Count);
                    currentElements.Add(baseIntegralElements[elementIndex]);
                    baseIntegralElements.RemoveAt(elementIndex);
                }

                if (currentElements.Count > 0)
                    areas.Add(currentElements);
            }

            if (baseIntegralElements.Count > 0)
            {
                int randomArea = this.BondRandomizer.Next(0, areas.Count);
                areas[randomArea].AddRange(baseIntegralElements);
            }

            return areas;
        }




    }
}
