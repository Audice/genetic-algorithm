﻿using GeneticAlgorithmB.BaseIntegralElements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithmB.Generator
{
    public class GeneratorBaseSet
    {
        uint NumElements = 0;
        public GeneratorBaseSet(uint numElements)
        {
            this.NumElements = numElements;
        }

        public List<IntegralElement> GetGeneratedElements()
        {
            List<IntegralElement> resultList = new List<IntegralElement>();
            for (uint i=0; i < this.NumElements; i++)
            {
                resultList.Add(new IntegralElement(i));
            }
            return resultList;
        }



    }
}
