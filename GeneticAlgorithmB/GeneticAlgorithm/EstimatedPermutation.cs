﻿using GeneticAlgorithmB.BaseIntegralElements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithmB.GeneticAlgorithm
{
    public class EstimatedPermutation
    {
        public List<uint> ElementsSet { get; private set; } = null;

        public ulong OptimalityAssessment { get; private set; } = ulong.MaxValue;

        public IntegralElementsSet BaseIntegralElementSet { get; private set; } = null;

        public EstimatedPermutation(List<uint> elementsSet, IntegralElementsSet baseIntegralElementSet)
        {
            this.ElementsSet = elementsSet;
            this.BaseIntegralElementSet = baseIntegralElementSet;
            CalculateOptimalityAssessment();

        }

        private void CalculateOptimalityAssessment()
        {
            if (this.BaseIntegralElementSet == null || this.ElementsSet == null) 
                throw new ArgumentException("Базовый набор не определён или нет списка элементов");

            this.OptimalityAssessment = 0;

            for (int i=0; i < this.BaseIntegralElementSet.AreasList.Count; i++)
            {
                List<uint> areaIndexes = new List<uint>();
                for (int j = 0; j < this.BaseIntegralElementSet.AreasList[i].Count; j++)
                {
                    long index = this.ElementsSet.FindIndex(x => x == this.BaseIntegralElementSet.AreasList[i][j]);
                    if (index >= 0)
                    {
                        areaIndexes.Add((uint)index);
                    }
                }
                if (areaIndexes.Count >= 2)
                {
                    uint maxIndex = areaIndexes.Max();
                    uint minIndex = areaIndexes.Min();
                    ulong resultAssessment = (ulong)((maxIndex - minIndex + 1) - areaIndexes.Count);
                    this.OptimalityAssessment += resultAssessment;
                }
                else
                {
                    this.OptimalityAssessment = ulong.MaxValue;
                    break;
                }
            }
        }
    }
}
