﻿using GeneticAlgorithmB.BaseIntegralElements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithmB.GeneticAlgorithm
{
    public class GenerationGenerator
    {
        public uint NumberIndividuals { get; private set; } = 0;

        public List<List<uint>> IndividualsList { get; private set; } = null;

        public double MaxNumIndividuals {
            get { return Factorial(this.NumberIndividuals); }
        }

        IntegralElementsSet BaseIntegralElementsSet = null;


        Random Randomizer;

        List<uint> BaseIELocation = null;


        //Поля для генерации поколений
        /// <summary>
        /// Процент осыбей для мутации. 5% по умолчанию
        /// </summary>
        public uint MutationPercent { get; private set; } = 5;
        public uint IndividualMutationPercent { get; private set; } = 10;


        //Лучшая оценка
        public ulong BestGrade { get; private set; } = ulong.MaxValue;

        public uint NumGeneration { get; private set; } = 0;

        public uint NumberStagnantGenerations {get; private set;} = 0;

        public const uint MaxNumStagnantGenerations = 1000;

        private uint countGeneration = 0;
        public uint CountGeneration {
            get { return countGeneration; }
            private set
            {
                if (value != countGeneration)
                {
                    countGeneration = value;
                    CountGenerationChange?.Invoke(this, EventArgs.Empty);
                }
                
            }
        }
        public EventHandler<EventArgs> CountGenerationChange;



        public GenerationGenerator(uint numIndividuals, IntegralElementsSet set, 
            uint mutationPercent, uint individualMutationPercent)
        {
            this.NumberIndividuals = numIndividuals;
            this.IndividualsList = new List<List<uint>>();
            this.BaseIntegralElementsSet = set;

            if (this.NumberIndividuals > 1)
            {
                this.IndividualMutationPercent = individualMutationPercent;
                this.MutationPercent = mutationPercent;
                this.Randomizer = new Random();
                this.BaseIELocation = new List<uint>();
                for (int i = 0; i < set.IntegralElementsList.Count; i++)
                    this.BaseIELocation.Add(set.IntegralElementsList[i].IntegralElementNumber);

                SearchBestGeneration();
            }
            else
            {
                throw new ArgumentException("Слишком малая популяция");
            }
        }

        private void SearchBestGeneration()
        {
            CountGeneration = 0;

            ///Цикл по обновлению популяции. Реализовать!!!!!
            GeneratePopulation();
            var childrens = Crossover();
            var mutants = Mutation(Selection(childrens));
            //Сваливаем в кучку
            this.IndividualsList.AddRange(childrens);
            this.IndividualsList.AddRange(mutants);

            OptimalIndividuals optimalIndividuals = new OptimalIndividuals(this.NumberIndividuals, IndividualsList, BaseIntegralElementsSet);
            var bestList = optimalIndividuals.GetOptimalPopulation();
            this.BestGrade = optimalIndividuals.BestGrade;

            this.IndividualsList = new List<List<uint>>(bestList);

            CountGeneration++;

            while (NumberStagnantGenerations < MaxNumStagnantGenerations && this.BestGrade != 0)
            {
                ulong currentGrade = ulong.MaxValue;

                var childrensNext = Crossover();
                var mutantsNext = Mutation(Selection(childrens));
                //Сваливаем в кучку
                this.IndividualsList.AddRange(childrensNext);
                this.IndividualsList.AddRange(mutantsNext);

                OptimalIndividuals optimalIndividualsNext = new OptimalIndividuals(this.NumberIndividuals, IndividualsList, BaseIntegralElementsSet);
                var bestListNext = optimalIndividualsNext.GetOptimalPopulation();

                currentGrade = optimalIndividualsNext.BestGrade;

                this.IndividualsList = new List<List<uint>>(bestListNext);

                if (currentGrade <= this.BestGrade)
                {
                    NumberStagnantGenerations++;
                }
                else
                {
                    this.BestGrade = currentGrade;
                    NumberStagnantGenerations = 0;
                }

                CountGeneration++;
            }
        }


        List<List<uint>> Mutation(List<List<uint>> children)
        {
            List<List<uint>> mutants = new List<List<uint>>(children);
            List<List<uint>> resultMutants = new List<List<uint>>();
            //Производим рандомный swap
            uint countMutationElements = (uint)((double)children.Count * ((double)this.IndividualMutationPercent / 100.0));
            if (countMutationElements < children.Count && countMutationElements > 0)
            {
                for (int i = 0; i < mutants.Count; i++)
                {
                    for (int j = 0; j < countMutationElements; j++)
                    {
                        int firstIndex = this.Randomizer.Next(0, mutants[i].Count);
                        int secondIndex = this.Randomizer.Next(0, mutants[i].Count - 1);
                        if (secondIndex == firstIndex)
                            secondIndex++;
                        //swap
                        uint tmp = mutants[i][firstIndex];
                        mutants[i][firstIndex] = mutants[i][secondIndex];
                        mutants[i][secondIndex] = tmp;
                    }
                }
                return mutants;
            }
            return new List<List<uint>>();
        }

        /// <summary>
        /// Формирование потомков
        /// </summary>
        private List<List<uint>> Crossover()
        {
            List<List<uint>> childrens = new List<List<uint>>();
            for (int i=0; i < this.IndividualsList.Count; i += 2)
            {
                childrens.AddRange(CrossoverMethod(IndividualsList[i], IndividualsList[i+1]));
            }
            return childrens;
        }

        List<List<uint>> Selection(List<List<uint>> childrens)
        {
            List<List<uint>> mutatedChildrens = new List<List<uint>>();
            uint countMutation = (uint)((double)childrens.Count * ((double)this.MutationPercent / 100.0));
            if (countMutation > 0 && countMutation < childrens.Count)
            {
                List<int> indexs = new List<int>();
                for (int i = 0; i < countMutation; i++)
                {
                    int randomIndex = this.Randomizer.Next(0, childrens.Count);
                    if (indexs.FindIndex(x => x == randomIndex) < 0)
                    {
                        indexs.Add(randomIndex);
                    }
                    else
                    {
                        i--;
                    }
                }
                for (int i = 0; i < indexs.Count; i++)
                {
                    mutatedChildrens.Add(childrens[indexs[i]]);
                }
            }
            return mutatedChildrens;

        }



        //Вернём двух потомков
        private List<List<uint>> CrossoverMethod(List<uint> firstParent, List<uint> secondParent)
        {
            //В качестве метода кросовера, реализуем механизм swap, 
            //позволяющий сделать из первого родителя второго, 
            //за конечное число перестановок 
            List<List<uint>> children = new List<List<uint>>();
            List<uint> copyFirstParent = new List<uint>(firstParent);
            for (int i = 0; i < copyFirstParent.Count; i++)
            {
                int indexFirstParent = copyFirstParent.FindIndex(x => x == (i));
                int indexSecondParent = secondParent.FindIndex(x => x == (i));
                uint tmpValue = copyFirstParent[indexSecondParent];
                copyFirstParent[indexSecondParent] = secondParent[indexSecondParent];
                copyFirstParent[indexFirstParent] = tmpValue;
                children.Add(new List<uint>(copyFirstParent));
            }
            int countChildren = 0;
            List<List<uint>> returnChildren = new List<List<uint>>();
            while (countChildren < 2)
            {
                int randomIndex = this.Randomizer.Next(0, children.Count);
                returnChildren.Add(children[randomIndex]);
                countChildren++;
            }

            return returnChildren;
        }

        /// <summary>
        /// Поиск максимального числа для текущего поколения
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        double Factorial(uint n)
        {
            double result = 1;
            for (int i = 2; i <= n; i++)
                result = result * i;
            return result;
        }

        /// <summary>
        /// Генерация популяции из всевозможных наборов
        /// </summary>
        void GeneratePopulation()
        {
            ulong numberIndividualsPopulation = 0;
            while (numberIndividualsPopulation < this.NumberIndividuals)
            {
                //Алгоритм генерации поколения
                List<uint> localIEList = new List<uint>(this.BaseIELocation);
                List<uint> possibleIndividual = new List<uint>();

                while (localIEList.Count > 0)
                {
                    int randomIndex = this.Randomizer.Next(0, localIEList.Count);
                    possibleIndividual.Add(localIEList[randomIndex]);
                    localIEList.RemoveAt(randomIndex);
                }

                if (CheckIndividuals(possibleIndividual)) //Иначе снова генирируем перестановку
                {
                    this.IndividualsList.Add(possibleIndividual);
                    numberIndividualsPopulation++;
                }
            }
        }

        /// <summary>
        /// Проверка текущего набора элементов, во избежания кратных????
        /// </summary>
        /// <returns>True - принята к публикации</returns>
        bool CheckIndividuals(List<uint> individuals)
        {
            if (this.IndividualsList.Count == 0) return true;
            bool isEqualFind = false;
            for (int i=0; i < this.IndividualsList.Count; i++)
            {
                int j = 0;
                for (j=0; j < individuals.Count; j++)
                {
                    if (this.IndividualsList[i][j] != individuals[j])
                        break;
                }
                if (individuals.Count == j)
                {
                    isEqualFind = true;
                    break;
                }
            }
            return isEqualFind ? false : true;
        }



    }
}
