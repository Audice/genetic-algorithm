﻿using GeneticAlgorithmB.BaseIntegralElements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GeneticAlgorithmB.GeneticAlgorithm
{
    public class OptimalIndividuals
    {
        public uint NumberIndividuals { get; private set; } = 0;

        public List<List<uint>> IndividualsList { get; private set; } = null;

        IntegralElementsSet BaseIntegralElementsSet = null;

        List<EstimatedPermutation> RatingList;

        public ulong BestGrade { get; private set; } = ulong.MaxValue;

        public OptimalIndividuals(uint numberIndividuals, List<List<uint>> individualsList, IntegralElementsSet baseIntegralElementsSet)
        {
            this.NumberIndividuals = numberIndividuals;
            this.IndividualsList = new List<List<uint>>(individualsList);
            this.BaseIntegralElementsSet = baseIntegralElementsSet;
            this.RatingList = new List<EstimatedPermutation>();
            BuildOptimalIntegralSet();
            this.BestGrade = RatingList[0].OptimalityAssessment;
        }

        public List<List<uint>> GetOptimalPopulation()
        {
            List<List<uint>> resultsList = new List<List<uint>>();
            uint currentCount = 0;
            if (this.RatingList.Count >= this.NumberIndividuals)
                currentCount = this.NumberIndividuals;
            else
                currentCount = (uint)this.RatingList.Count;
            for (int i = 0; i < currentCount; i++)
                resultsList.Add(this.RatingList[i].ElementsSet);
            return resultsList;
        }

        void BuildOptimalIntegralSet()
        {
            /////Функция оценки - оценивать разность между индексами крайним правим и крайним левым для данной области
            for (int i=0; i < this.IndividualsList.Count; i++)
            {
                EstimatedPermutation currentPermition = new EstimatedPermutation(this.IndividualsList[i], this.BaseIntegralElementsSet);
                InseartEstimatedPermutation(currentPermition);
            }
        }

        void InseartEstimatedPermutation(EstimatedPermutation currentPermition)
        {
            if (this.RatingList == null) this.RatingList = new List<EstimatedPermutation>();
            if (this.RatingList.Count == 0)
            {
                this.RatingList.Add(currentPermition);
                return;
            }
            int i = 0;
            for (i = 0; i < this.RatingList.Count; i++)
            {
                if (this.RatingList[i].OptimalityAssessment > currentPermition.OptimalityAssessment)
                {
                    this.RatingList.Insert(i, currentPermition);
                    break;
                }
            }
            if (i == this.RatingList.Count) this.RatingList.Add(currentPermition);
        }

    }
}
