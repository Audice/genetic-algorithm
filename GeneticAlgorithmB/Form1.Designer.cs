﻿namespace GeneticAlgorithmB
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.generationCount = new System.Windows.Forms.TextBox();
            this.populationSize = new System.Windows.Forms.TextBox();
            this.individualsMutants = new System.Windows.Forms.TrackBar();
            this.populationMutants = new System.Windows.Forms.TrackBar();
            this.NumElements = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.BaseIntegralSet = new System.Windows.Forms.FlowLayoutPanel();
            this.IndividualIntegralSet = new System.Windows.Forms.FlowLayoutPanel();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.individualsMutants)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.populationMutants)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.generationCount);
            this.groupBox1.Controls.Add(this.populationSize);
            this.groupBox1.Controls.Add(this.individualsMutants);
            this.groupBox1.Controls.Add(this.populationMutants);
            this.groupBox1.Controls.Add(this.NumElements);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(10, 11);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Size = new System.Drawing.Size(250, 593);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Базовые настройки";
            // 
            // generationCount
            // 
            this.generationCount.Enabled = false;
            this.generationCount.Location = new System.Drawing.Point(145, 122);
            this.generationCount.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.generationCount.Name = "generationCount";
            this.generationCount.Size = new System.Drawing.Size(86, 20);
            this.generationCount.TabIndex = 4;
            // 
            // populationSize
            // 
            this.populationSize.Location = new System.Drawing.Point(145, 77);
            this.populationSize.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.populationSize.Name = "populationSize";
            this.populationSize.Size = new System.Drawing.Size(86, 20);
            this.populationSize.TabIndex = 4;
            // 
            // individualsMutants
            // 
            this.individualsMutants.Location = new System.Drawing.Point(7, 284);
            this.individualsMutants.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.individualsMutants.Maximum = 100;
            this.individualsMutants.Name = "individualsMutants";
            this.individualsMutants.Size = new System.Drawing.Size(224, 45);
            this.individualsMutants.TabIndex = 3;
            this.individualsMutants.Value = 10;
            // 
            // populationMutants
            // 
            this.populationMutants.Location = new System.Drawing.Point(7, 198);
            this.populationMutants.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.populationMutants.Maximum = 100;
            this.populationMutants.Minimum = 1;
            this.populationMutants.Name = "populationMutants";
            this.populationMutants.Size = new System.Drawing.Size(224, 45);
            this.populationMutants.TabIndex = 2;
            this.populationMutants.Value = 5;
            // 
            // NumElements
            // 
            this.NumElements.Location = new System.Drawing.Point(145, 30);
            this.NumElements.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.NumElements.Name = "NumElements";
            this.NumElements.Size = new System.Drawing.Size(86, 20);
            this.NumElements.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 255);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(164, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Процент мутации индивидуума";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 171);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(185, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Процент мутаций среди потомства";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 122);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(123, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Количество поколений";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 77);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Размер популяции";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 23);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(127, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "Количество элементов \r\nинтегральной схемы";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(265, 10);
            this.button1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(200, 50);
            this.button1.TabIndex = 1;
            this.button1.Text = "Сгенерировать схему";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // BaseIntegralSet
            // 
            this.BaseIntegralSet.AutoScroll = true;
            this.BaseIntegralSet.Location = new System.Drawing.Point(265, 65);
            this.BaseIntegralSet.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.BaseIntegralSet.Name = "BaseIntegralSet";
            this.BaseIntegralSet.Size = new System.Drawing.Size(200, 537);
            this.BaseIntegralSet.TabIndex = 2;
            // 
            // IndividualIntegralSet
            // 
            this.IndividualIntegralSet.Location = new System.Drawing.Point(469, 66);
            this.IndividualIntegralSet.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.IndividualIntegralSet.Name = "IndividualIntegralSet";
            this.IndividualIntegralSet.Size = new System.Drawing.Size(193, 537);
            this.IndividualIntegralSet.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(750, 613);
            this.Controls.Add(this.IndividualIntegralSet);
            this.Controls.Add(this.BaseIntegralSet);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.individualsMutants)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.populationMutants)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.FlowLayoutPanel BaseIntegralSet;
        private System.Windows.Forms.FlowLayoutPanel IndividualIntegralSet;
        private System.Windows.Forms.TextBox NumElements;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TrackBar populationMutants;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TrackBar individualsMutants;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox populationSize;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox generationCount;
        private System.Windows.Forms.Label label5;
    }
}

