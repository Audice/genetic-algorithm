﻿using GeneticAlgorithmB.Generator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithmB.BaseIntegralElements
{
    public class IntegralElementsSet
    {
        public List<IntegralElement> IntegralElementsList { get; private set; } = null;

        /// <summary>
        /// Генератор исходного набора данных
        /// </summary>
        GeneratorBaseSet GeneratorBaseSet = null;

        //Матрица смежности для хранения связей. Двумерный массив = симмктричная матрица. Просто, но работает
        //   1  2  3  4
        //
        //1  1  0  1  1
        //2  0  1  1  0
        //3  1  1  1  1
        //4  1  0  1  1
        public byte[,] AdjacencyMatrix { get; private set; } = null;

        /// <summary>
        /// Список областей между которыми есть связь
        /// </summary>
        public List<List<uint>> AreasList { get; private set; } = null;



        public IntegralElementsSet(uint numIntegralElements)
        {
            GeneratorBaseSet = new GeneratorBaseSet(numIntegralElements);
            IntegralElementsList = GeneratorBaseSet.GetGeneratedElements();
            GenerateAdjacencyMatrix(numIntegralElements);
        }

        public IntegralElementsSet(List<List<uint>> areasList, List<uint> elements)
        {
            IntegralElementsList = new List<IntegralElement>();
            for (int i = 0; i < elements.Count; i++)
            {
                IntegralElementsList.Add(new IntegralElement(elements[i]));
            }
            AreasList = new List<List<uint>>(areasList);
        }

        void GenerateAdjacencyMatrix(uint numIntegralElements)
        {
            BondGenerator bondGenerator = new BondGenerator(numIntegralElements);
            this.AdjacencyMatrix = bondGenerator.GenerateAdjacencyMatrix();
            this.AreasList = bondGenerator.Areas;
        }



    }
}
