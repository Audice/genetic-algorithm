﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithmB.BaseIntegralElements
{
    public class IntegralElement
    {
        public uint IntegralElementNumber { get; private set; }

        public uint ContBondedElement { get; set; }

        public IntegralElement(uint integralElementNumber)
        {
            this.IntegralElementNumber = integralElementNumber;
        }


    }
}
