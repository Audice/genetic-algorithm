﻿using GeneticAlgorithmB.BaseIntegralElements;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GeneticAlgorithmB.Visualiser
{
    public class Visualizer
    {
        const int Widht = 70;
        const int Height = 50;

        /// <summary>
        /// Элементы интегральной схемы
        /// </summary>
        List<Bitmap> PreviosFrame;

        FlowLayoutPanel ElementsPanel = null;

        IntegralElementsSet IntegralElementsSet = null;

        Random Randomizer;

        public Visualizer(FlowLayoutPanel elementsPanel, IntegralElementsSet integralElementsSet)
        {
            if (elementsPanel == null || integralElementsSet == null) throw new ArgumentException("Не проинициализированный список интегральных элементов");
            Randomizer = new Random();
            this.ElementsPanel = elementsPanel;
            this.IntegralElementsSet = integralElementsSet;
            Bitmap bitmap = new Bitmap(Widht * 3, Height * this.IntegralElementsSet.IntegralElementsList.Count + 10);
            DrawIntegralElements(bitmap);
            DrawBond(bitmap);
        }

        private void DrawIntegralElements(Bitmap bitmap)
        {
            if (IntegralElementsSet == null) throw new ArgumentException("Не проинициализированный список интегральных элементов");
            // Create pen.
            Pen blackPen = new Pen(Color.Black, 3);
            // Draw line to screen.
            using (Graphics g = Graphics.FromImage(bitmap))
            {
                for (int i = 0; i < this.IntegralElementsSet.IntegralElementsList.Count; i++)
                {
                    g.DrawRectangle(blackPen, 0, i * Height, Widht, Height);
                    g.DrawString(this.IntegralElementsSet.IntegralElementsList[i].IntegralElementNumber.ToString(), 
                        new Font("Arial", 14), new SolidBrush(Color.Black), 0, i * Height);
                }
            }
            ElementsPanel.Controls.Clear();
            ElementsPanel.Controls.Add(new PictureBox() { 
                Image = bitmap, Size = bitmap.Size
            });
        }


        private void DrawBond(Bitmap bitmap)
        {
            if (IntegralElementsSet == null) throw new ArgumentException("Не проинициализированный список интегральных элементов");
            int bondeCounter = 5;
            int outLine = 1;
            var areasElements = this.IntegralElementsSet.AreasList;
            using (Graphics g = Graphics.FromImage(bitmap))
            {
                for (int i = 0; i < areasElements.Count; i++)
                {
                    var color = String.Format("#{0:X6}", Randomizer.Next(0x1000000));
                    Pen randomPen = new Pen(System.Drawing.ColorTranslator.FromHtml(color), 2);

                    for (int j = 0; j < areasElements[i].Count; j++)
                    {
                        for (int k = j + 1; k < areasElements[i].Count; k++)
                        {
                            int indexFirst = this.IntegralElementsSet.IntegralElementsList.FindIndex(x => x.IntegralElementNumber == areasElements[i][j]);
                            int indexSecond = this.IntegralElementsSet.IntegralElementsList.FindIndex(x => x.IntegralElementNumber == areasElements[i][k]);

                            g.DrawLine(randomPen, 
                                Widht + bondeCounter * outLine,
                                indexFirst * Height + Height / 2,
                                Widht + bondeCounter + bondeCounter * outLine,
                                indexFirst * Height + Height / 2);

                            g.DrawLine(randomPen, 
                                Widht + bondeCounter + bondeCounter * outLine,
                                indexFirst * Height + Height / 2,
                                Widht + bondeCounter + bondeCounter * outLine,
                                indexSecond * Height + Height / 2);

                            g.DrawLine(randomPen,
                                Widht + bondeCounter * outLine,
                                indexSecond * Height + Height / 2,
                                Widht + bondeCounter + bondeCounter * outLine,
                                indexSecond * Height + Height / 2);

                        }
                    }
                    outLine++;

                }
            }
            ElementsPanel.Controls.Clear();
            ElementsPanel.Controls.Add(new PictureBox() { 
                Image = bitmap, 
                Size = bitmap.Size
            });
        }



    }
}
