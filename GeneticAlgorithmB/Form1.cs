﻿using GeneticAlgorithmB.BaseIntegralElements;
using GeneticAlgorithmB.GeneticAlgorithm;
using GeneticAlgorithmB.Visualiser;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GeneticAlgorithmB
{
    public partial class Form1 : Form
    {
        IntegralElementsSet integralElementsSet = null;
        Visualizer visualiser = null;
        Visualizer visualiserBestChildren = null;
        GenerationGenerator GenerationGenerator;
        public Form1()
        {
            InitializeComponent();
            uint? numElement = 0;
            if ((numElement = GetNumElements(this.NumElements.Text)) != null)
            {
                integralElementsSet = new IntegralElementsSet(numElement.Value);
                visualiser = new Visualizer(this.BaseIntegralSet, integralElementsSet);
            }
        }


        uint? GetNumElements(string strNumElements)
        {
            uint result = 0;
            if (uint.TryParse(strNumElements, out result))
                return result;
            else
                return null;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            uint? numElement = 0;
            if ((numElement = GetNumElements(this.NumElements.Text)) != null)
            {
                integralElementsSet = new IntegralElementsSet(numElement.Value);
                uint? populationSize = GetNumElements(this.populationSize.Text);
                if (populationSize == null)
                {
                    MessageBox.Show("Error", "Размер популяции определён неправильно");
                    return;
                }
                GenerationGenerator = new GenerationGenerator(populationSize.Value, integralElementsSet, (uint)populationMutants.Value, (uint)individualsMutants.Value);
                visualiser = new Visualizer(this.BaseIntegralSet, integralElementsSet);
                IntegralElementsSet child = new IntegralElementsSet(integralElementsSet.AreasList, GenerationGenerator.IndividualsList[0]);
                visualiserBestChildren = new Visualizer(this.IndividualIntegralSet, child);
                this.generationCount.Text = GenerationGenerator.CountGeneration.ToString();
            }
        }
    }
}
